# StringLogin

Basically sniffs login codes from the account or generates string sessions.

 + `python3 stringlogin.py`
 + To obtain your string session, press Enter and follow on-terminal prompts.
 + To sniff login codes, enter your string session (then press Enter, obviously)
